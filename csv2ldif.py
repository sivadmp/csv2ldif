#!/usr/bin/env python
# coding: utf-8

import csv
import os
import base64
from passlib.hash import ldap_salted_sha1

# you can configure that
CSV_DELIMITER = ','
CSV_QUOTECHAR = '"'
ROLE_SEPARATOR = ';'
LDAP_ROOT_DN = 'dc=sivadmp,dc=net'
EOL = '\n'
USERS_FILE = 'data/usuarios.csv'
HEADER = True

errors = []


def printOrgUnitDefinition():
    '''Print OU definitions'''
    users_def = ['# usuarios, sivadmp.net',
                 'dn: ou=usuarios,' + LDAP_ROOT_DN,
                 'objectClass: organizationalUnit',
                 'objectClass: top',
                 'ou: usuarios']
    print(EOL.join(users_def) + EOL)


def getField(rownb, value, fieldname):
    if value == '':
        # raise Exception("Empty " + fieldname + " on row " + str(rownb))
        errors.append("Empty " + fieldname + " on row " + str(rownb))
    return value


def main():
    rownb = 0

    if os.path.isfile(USERS_FILE):
        inputUsersReader = csv.reader(
            open(USERS_FILE, 'r'), delimiter=CSV_DELIMITER, quotechar=CSV_QUOTECHAR)

        if HEADER:
            next(inputUsersReader)

        for row in inputUsersReader:
            employeeNumber = row[0].strip()  # user id
            uid = getField(rownb, row[1].strip(), 'uid')  # login
            userPassword = ldap_salted_sha1.hash(
                getField(rownb, row[2].strip(), 'userPassword'))  # password
            mail = getField(rownb, row[3].strip(), 'mail')  # email
            if not mail.strip():
                uid + '@empty.com'
            givenName = row[4].strip()  # first name
            sn = row[5].strip()  # second name
            if not sn.strip():
                sn = uid
            o = row[6].strip()  # org / company
            title = row[7].strip()  # job situation
            telephoneNumber = row[8].strip()  # phone number
            postalAddress = row[9].strip()  # address

            user = [
                '# usuario, ' + uid,
                'dn: uid=' + uid + ',ou=usuarios,' + LDAP_ROOT_DN,
                'objectClass: inetOrgPerson',
                'objectClass: organizationalPerson',
                'objectClass: person',
                'objectClass: shadowAccount',
                'objectClass: top',
                'uid: ' + uid,
                'userPassword: ' + userPassword,
                'sn: ' + sn,
                'cn: ' + givenName + ' ' + sn,
                'mail: ' + mail,
                'o: ' + o
            ]

            if employeeNumber.strip() != '':
                user.append('employeeNumber: ' + employeeNumber)
            if givenName.strip() != '':
                user.append('givenName: ' + givenName)
                user.append('displayName: ' + givenName)
            if title.strip() != '':
                user.append('title: ' + title)
            if telephoneNumber.strip() != '':
                user.append('telephoneNumber: ' + telephoneNumber)
            if postalAddress.strip() != '':
                user.append(
                    'postalAddress:: ' + base64.b64encode(postalAddress.encode('windows-1252')).decode('utf8'))
            print(EOL.join(user) + EOL)

            rownb += 1

printOrgUnitDefinition()
main()
# print(errors)
