# CSV to LDIF

Script para convertir archivos CSV a LDIF

## Dependencias

* python3

```bash
sudo apt install python3-passlib
```

## Ejecución

```bash
python3 csv2ldif.py  > usuarios.ldif
```

